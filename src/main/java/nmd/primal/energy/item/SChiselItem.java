package nmd.primal.energy.item;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import nmd.primal.energy.block.ModBlocks;
import nmd.primal.energy.common.ModInfo;
import nmd.primal.energy.tileents.TileEntLatheBase;
import nmd.primal.energy.util.CustomTab;
import cpw.mods.fml.common.registry.GameRegistry;

public class SChiselItem extends Item{

	private String name = "schiselItem";
	private Item item;
	private boolean doWork;

	public SChiselItem(){
		setMaxStackSize(1);
		setUnlocalizedName(name);
		setTextureName(ModInfo.MOD_ID + ":" + name);
		setMaxDamage(100);
		setNoRepair();
		setCreativeTab(CustomTab.NMDEnergyTab);

		item = this;
		GameRegistry.registerItem(this, name);
	}

	@Override
	public boolean doesContainerItemLeaveCraftingGrid(ItemStack itemstack) {
		return false;
	}

	@Override
	public Item getContainerItem()
	{
		item.setDamage(new ItemStack(item), +1);
		return item;
	}



}
