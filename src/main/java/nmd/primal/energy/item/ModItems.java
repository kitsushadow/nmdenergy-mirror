package nmd.primal.energy.item;

import net.minecraft.item.Item;
import nmd.primal.energy.common.ModInfo;
import nmd.primal.energy.util.CustomTab;
import cpw.mods.fml.common.registry.GameRegistry;

public class ModItems {

	public static Item schiselItem;
	public static Item swheelItem;
	public static Item sgearItem;
	public static Item saxleItem;
	
	
	public static void registerItems(){
		
		schiselItem = new SChiselItem();
				
		swheelItem = new Item().setUnlocalizedName("swheelItem").setCreativeTab(CustomTab.NMDEnergyTab).setTextureName(ModInfo.MOD_ID + ":swheelItem");
		GameRegistry.registerItem(swheelItem, "swheelItem");
		
		sgearItem = new Item().setUnlocalizedName("sgearItem").setCreativeTab(CustomTab.NMDEnergyTab).setTextureName(ModInfo.MOD_ID + ":sgearItem");
		GameRegistry.registerItem(sgearItem, "sgearItem");

		saxleItem = new Item().setUnlocalizedName("saxleItem").setCreativeTab(CustomTab.NMDEnergyTab).setTextureName(ModInfo.MOD_ID + ":saxleItem");
		GameRegistry.registerItem(saxleItem, "saxleItem");
	}
	
	
}
