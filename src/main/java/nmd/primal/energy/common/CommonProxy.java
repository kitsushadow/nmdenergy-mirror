package nmd.primal.energy.common;

import nmd.primal.energy.block.ModBlocks;
import nmd.primal.energy.crafting.ModCrafting;
import nmd.primal.energy.item.ModItems;
import nmd.primal.energy.render.RenderID;
import nmd.primal.energy.tileents.TileRegistry;
import nmd.primal.energy.util.CustomTab;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

public class CommonProxy {
	
	public void preInit(FMLPreInitializationEvent event) 
	{
		CustomTab.NMDEnergyTab();
		


	}

	public void init(FMLInitializationEvent event) 
	{
		ModBlocks.init();
		ModCrafting.init();
		RenderID.init();
		TileRegistry.init();
	}

	public void postInit(FMLPostInitializationEvent event) 
	{

	}


}
