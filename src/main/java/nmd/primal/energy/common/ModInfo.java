package nmd.primal.energy.common;

public class ModInfo {
	
	public static final String MOD_ID = "energy";
	public static final String MOD_NAME = "NMD Energy";
	public static final String MOD_PREFIX = "nmd";
	public static final String MOD_CHANNEL = "nmd-energy";
	public static final String MOD_VERSION = "0.0";
	
	public static final String COMMON_PROXY = "nmd.primal.energy.common.CommonProxy";
	public static final String CLIENT_PROXY = "nmd.primal.energy.client.ClientProxy";
	
	public static final int SMBBlock_ID = 201;
}
