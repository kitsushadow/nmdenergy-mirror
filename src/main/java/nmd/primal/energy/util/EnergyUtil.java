package nmd.primal.energy.util;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class EnergyUtil {
	
	public static int determineOrientationComplex(World world, int x, int y, int z, EntityLivingBase player)
	{
		if (MathHelper.abs((float)player.posX - (float)x) < 2.0F && MathHelper.abs((float)player.posZ - (float)z) < 2.0F)
		{
			double d0 = player.posY + 1.82D - (double)player.yOffset;
			if (d0 - (double)y > 2.0D){return 1;}
			if ((double)y - d0 > 0.0D) {return 0;}
		}

		int l = MathHelper.floor_double((double)(player.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
		return l == 0 ? 2 : (l == 1 ? 5 : (l == 2 ? 3 : (l == 3 ? 4 : 0)));
	}
	
	public static int determineOrientationSimple(EntityLivingBase player){
		int l = (MathHelper.floor_double((player.rotationYaw * 4F) / 360F + 0.5D) & 3);
		return l;
	}
}
