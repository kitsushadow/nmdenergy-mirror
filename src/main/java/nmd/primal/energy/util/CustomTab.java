package nmd.primal.energy.util;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

public class CustomTab {

	public static CreativeTabs NMDEnergyTab = new CreativeTabs("NMDEnergyTab") {
		@Override
		public Item getTabIconItem() {
			return Items.blaze_powder;
		}
	};

	public static void NMDEnergyTab() {

	}
}
