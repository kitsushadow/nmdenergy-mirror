package nmd.primal.energy.util;

import java.util.Hashtable;

import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import nmd.primal.energy.item.ModItems;

public interface LatheRecipes {
	
	static final Hashtable<Item, ItemStack> lathe = new Hashtable <Item, ItemStack>(){{

		//put(ModItems.ironAxeForm, new ItemStack(ModItems.ironaxeHead, 1));
		put(Item.getItemFromBlock(Blocks.wooden_slab), new ItemStack(ModItems.swheelItem));
		put(Item.getItemFromBlock(Blocks.planks), new ItemStack(ModItems.saxleItem));
		
	}};
	
	static final Hashtable<Item, Item> latheCheck = new Hashtable <Item, Item>(){{

		//put(ModItems.ironAxeForm, new ItemStack(ModItems.ironaxeHead, 1));
		put(ModItems.swheelItem, Item.getItemFromBlock(Blocks.wooden_slab));
		put(ModItems.saxleItem, Item.getItemFromBlock(Blocks.planks));
		
	}};

}
