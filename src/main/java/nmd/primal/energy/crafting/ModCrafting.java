package nmd.primal.energy.crafting;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import nmd.primal.energy.block.ModBlocks;
import nmd.primal.energy.item.ModItems;
import cpw.mods.fml.common.registry.GameRegistry;


public class ModCrafting {

	public static final void init() 
	{
		//GameRegistry.addRecipe(new ItemStack(ModBlocks.mineralBlock), new Object[] {"aa", "aa" , 'a', ModItems.mineraldustItem});
		
		GameRegistry.addRecipe(
				new ItemStack(ModItems.schiselItem), new Object[]{
					"  f",
					" t ",
					"s  ",
					'f', Items.flint, 't', Items.string, 's', Items.stick});
		
		GameRegistry.addRecipe(
				new ItemStack(ModItems.sgearItem), new Object[]{
					"x",
					"y",
					'x', new ItemStack(ModItems.schiselItem, 1, OreDictionary.WILDCARD_VALUE), 'y', ModItems.swheelItem});
		
		GameRegistry.addRecipe(
				new ItemStack(ModBlocks.SMBBlock), new Object[]{
					"sss",
					"s s",
					"sss",
					's', Blocks.wooden_slab});
		
		GameRegistry.addRecipe(
				new ItemStack(ModBlocks.crankBlock), new Object[]{
					" fs",
					" s ",
					" s ",
					'f', Items.string, 's', Items.stick});
				
	}
}
