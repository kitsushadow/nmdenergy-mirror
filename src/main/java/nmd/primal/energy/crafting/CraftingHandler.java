package nmd.primal.energy.crafting;

import net.minecraft.item.ItemStack;
import nmd.primal.energy.item.ModItems;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;

public class CraftingHandler {

	@SubscribeEvent
	public void onItemCrafting(PlayerEvent.ItemCraftedEvent event){

		for (int i = 0; i < event.craftMatrix.getSizeInventory(); i++) { // Checks all the slots 

			if (event.craftMatrix.getStackInSlot(i) != null) {
				ItemStack j = event.craftMatrix.getStackInSlot(i);

				if (j.getItem() != null && j.getItem() == ModItems.schiselItem) {
					ItemStack k = new ItemStack(ModItems.schiselItem, 2, (j.getItemDamage() + 1));
					if (k.getItemDamage() >= k.getMaxDamage()) {
						k.stackSize--;
					}
					event.craftMatrix.setInventorySlotContents(i, k);
				}
			}
		}
	}
}
