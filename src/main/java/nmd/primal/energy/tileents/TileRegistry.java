package nmd.primal.energy.tileents;

import cpw.mods.fml.common.registry.GameRegistry;

public class TileRegistry {
	
	public static final void init() {
		
        GameRegistry.registerTileEntity(TileEntCrank.class, "TileEntCrank");
        GameRegistry.registerTileEntity(TileEntLatheBase.class, "TileEntLatheBase");
	}
}
