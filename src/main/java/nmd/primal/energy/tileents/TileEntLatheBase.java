package nmd.primal.energy.tileents;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;
import nmd.primal.energy.item.ModItems;
import nmd.primal.energy.util.LatheRecipes;

public class TileEntLatheBase extends TileEntity implements IInventory {

	private ItemStack[] inv;
	private int i;
	public boolean isPowered = true;
	public int power, workCount;
	protected String specName = "TileEntLatheBase";
	public float rot = 0;

	public TileEntLatheBase() {
		this.inv = new ItemStack[1];
	}

	@Override
	public int getSizeInventory() {
		return this.inv.length;
	}

	@Override
	public ItemStack getStackInSlot(int slot) {
		return this.inv[slot];
	}

	public void markForUpdate(){
		worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
	}

	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt);
		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, -998, nbt);
	}

	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt) {
		readFromNBT(pkt.func_148857_g());
		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
	}

	@Override
	public void updateEntity() {
		World world = this.getWorldObj();
		int x = this.xCoord;
		int y = this.yCoord;
		int z = this.zCoord;
		if(!world.isRemote){
			if(this.isPowered==true){
				this.power--;
				rot-=45;
				System.out.println(rot);
				if(rot>=360||rot<=-360){
					rot=0;
				}
				if(power<=0){
					this.isPowered=false;
					this.power=0;
				}
				this.markForUpdate();
				this.markDirty();
			}
		}
		if (worldObj.isRemote) return;
	}



	@Override
	public ItemStack decrStackSize(int slot, int amount) {
		if (this.inv[slot] != null)
		{
			ItemStack itemstack;

			if (this.inv[slot].stackSize <= amount)
			{
				itemstack = this.inv[slot];
				this.inv[slot] = null;
				this.markForUpdate();
				this.markDirty();
				return itemstack;
			}
			itemstack = this.inv[slot].splitStack(amount);

			if (this.inv[slot].stackSize == 0)
			{
				this.inv[slot] = null;
			}
			this.markForUpdate();
			this.markDirty();
			return itemstack;
		}
		this.markForUpdate();
		this.markDirty();
		return null;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int slot) {
		if (this.inv[slot] != null)
		{
			ItemStack itemstack = this.inv[slot];
			this.inv[slot] = null;
			this.markForUpdate();
			this.markDirty();
			return itemstack;
		}
		else
		{
			this.markForUpdate();
			this.markDirty();
			return null;
		}
	}

	@Override
	public void setInventorySlotContents(int slot, ItemStack stack) {
		this.inv[slot] = stack;

		if (stack != null && stack.stackSize > this.getInventoryStackLimit())
		{
			stack.stackSize = this.getInventoryStackLimit();
		}
		this.markForUpdate();
		this.markDirty();
	}

	@Override
	public String getInventoryName() {
		return this.hasCustomInventoryName() ? this.specName : this.specName;
	}

	@Override
	public boolean hasCustomInventoryName() {
		return this.specName != null && this.specName.length() > 0;
	}

	@Override
	public int getInventoryStackLimit() {
		return 1;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false : player.getDistanceSq(this.xCoord + 0.5D, this.yCoord + 0.5D, this.zCoord + 0.5D) <= 64.0D;
	}

	@Override
	public void openInventory() {}

	@Override
	public void closeInventory() {}

	/*NEEDS TO CHECK AGAINST A SET OF DATA HOLDING LATHE TOOLS AND LATHE INPUTS
	 * */
	@Override
	public boolean isItemValidForSlot(int slot, ItemStack stack) {
		return false;
	}

	@Override
	public void readFromNBT(NBTTagCompound tagCompound)
	{
		super.readFromNBT(tagCompound);
		NBTTagList tagList = tagCompound.getTagList("Inventory", 10);
		this.inv = new ItemStack[this.getSizeInventory()];
		for (int i = 0; i < tagList.tagCount(); i++) {
			NBTTagCompound tag = tagList.getCompoundTagAt(i);
			byte slot = tag.getByte("Slot");
			if (slot >= 0 && slot < this.inv.length) {
				this.inv[slot] = ItemStack.loadItemStackFromNBT(tag);
			}
		}
		if (tagCompound.hasKey("CustomName", 8)) {
			this.specName = tagCompound.getString("CustomName");
		}
		this.isPowered = tagCompound.getBoolean("ISPOWERED");
		this.power = tagCompound.getInteger("POWER");
		this.workCount = tagCompound.getInteger("WORKCOUNT");
		this.rot = tagCompound.getFloat("ROT");
	}

	@Override
	public void writeToNBT(NBTTagCompound tagCompound)
	{
		super.writeToNBT(tagCompound);
		NBTTagList itemList = new NBTTagList();
		for (int i = 0; i < inv.length; i++) {
			if (inv[i] != null) {
				NBTTagCompound tag = new NBTTagCompound();
				tag.setByte("Slot", (byte) i);
				this.inv[i].writeToNBT(tag);
				itemList.appendTag(tag);
			}
		}
		tagCompound.setTag("Inventory", itemList);
		tagCompound.setBoolean("ISPOWERED", this.isPowered);
		tagCompound.setInteger("POWER", this.power);
		tagCompound.setInteger("WORKCOUNT", this.workCount);
		tagCompound.setFloat("ROT", this.rot);
	}


}
