package nmd.primal.energy;

import nmd.primal.energy.common.CommonProxy;
import nmd.primal.energy.common.ModInfo;
import nmd.primal.energy.crafting.CraftingHandler;
import nmd.primal.energy.item.ModItems;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = ModInfo.MOD_ID, name = ModInfo.MOD_NAME, version = ModInfo.MOD_VERSION)
public class Energy {

	@SidedProxy(clientSide = ModInfo.CLIENT_PROXY, serverSide = ModInfo.COMMON_PROXY)
	public static CommonProxy proxy;

	@Instance
	public static Energy instance = new Energy();

	@EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		Energy.proxy.preInit(event);
		ModItems.registerItems();
		// some example code
		// System.out.println("DIRT BLOCK >> "+Blocks.dirt.getUnlocalizedName());

	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		
		Energy.proxy.init(event);
		FMLCommonHandler.instance().bus().register(new CraftingHandler());
		
		// some example code
		// System.out.println("DIRT BLOCK >> "+Blocks.dirt.getUnlocalizedName());
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {

		Energy.proxy.postInit(event);
		System.out.println("U want some Body Massage?");
		// RenderingRegistry.registerEntityRenderingHandler(EntityShit.class,
		// new RenderSnowball(ModItems.itemShit));
	}
}
