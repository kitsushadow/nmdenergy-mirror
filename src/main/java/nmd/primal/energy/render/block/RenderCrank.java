package nmd.primal.energy.render.block;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import nmd.primal.energy.block.ModBlocks;
import nmd.primal.energy.tileents.TileEntCrank;

public class RenderCrank extends TileEntitySpecialRenderer {

	private int i = 0;
	private int a = 0;
	public static final ResourceLocation MODEL = new ResourceLocation("energy:models/Crank.obj");
	public static final ResourceLocation TEXTURE = new ResourceLocation("energy:models/Crank.png");
	private IModelCustom model = AdvancedModelLoader.loadModel(MODEL);

	@Override
	public void renderTileEntityAt(TileEntity tileEnt, double x, double y, double z, float scale) {

		GL11.glPushMatrix();

		GL11.glTranslatef((float) x, (float) y, (float) z);
		TileEntCrank tile = (TileEntCrank) tileEnt;

		if(tile.getBlockMetadata()==0){
			GL11.glRotatef(180, 0.0f, 0.0f, 1.0f);
			GL11.glTranslatef(-1.0f, -1.0f, -0.0f);
		}


		if(tile.getBlockMetadata()==2){
			GL11.glRotatef(-90, 1.0f, 0.0f, 0.0f);
			GL11.glTranslatef(0.0f, -1.0f, 0.0f);
		}
		if(tile.getBlockMetadata()==3){
			GL11.glRotatef(90, 1.0f, 0.0f, 0.0f);
			GL11.glTranslatef(0.0f, 0.0f, -1.0f);
		}
		if(tile.getBlockMetadata()==4){
			GL11.glRotatef(90, 0.0f, 0.0f, 1.0f);
			GL11.glTranslatef(0.0f, -1.0f, 0.0f);
		}
		if(tile.getBlockMetadata()==5){
			GL11.glRotatef(90, 0.0f, 0.0f, -1.0f);
			GL11.glTranslatef(-1.0f, 0.0f, 0.0f);
		}

		tile.markForUpdate();
		tile.markDirty();
		renderBlock(tile, tileEnt.getWorldObj(), tileEnt.xCoord,tileEnt.yCoord, tileEnt.zCoord, ModBlocks.crankBlock);
		GL11.glPopMatrix();
	}

	public void renderBlock(TileEntCrank tl, World world, int x, int y,int z, Block block) {

		GL11.glPushMatrix();

		float scale = 0.5F;
		GL11.glScalef(scale, scale, scale);

		GL11.glTranslatef(1.0F, 0.48F, 1.0F);
		
		if(tl.isPowered == true){

			GL11.glRotatef(i, 0.0F, 1.0F, 0.0F);
			a++;
			if(a==1){
				i=i+14;
				a=0;
			}
			if(i==360){
				i=0;
			}
		}

		tl.markForUpdate();
		tl.markDirty();
		FMLClientHandler.instance().getClient().renderEngine.bindTexture(TEXTURE);

		this.model.renderAll();
		GL11.glPopMatrix();
	}

}
