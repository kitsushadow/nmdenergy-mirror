package nmd.primal.energy.render.block;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import nmd.primal.energy.block.ModBlocks;
import nmd.primal.energy.item.ModItems;
import nmd.primal.energy.tileents.TileEntLatheBase;

public class RenderLathe extends TileEntitySpecialRenderer {

	public static final ResourceLocation MODEL = new ResourceLocation("energy:models/Lathe.obj");
	public static final ResourceLocation TEXTURE = new ResourceLocation("energy:models/FlintLathe.png");
	private IModelCustom model = AdvancedModelLoader.loadModel(MODEL);
	EntityItem entItem = null;

	@Override
	public void renderTileEntityAt(TileEntity tileEnt, double x, double y, double z, float scale) {
		TileEntLatheBase tile = (TileEntLatheBase) tileEnt;
		GL11.glPushMatrix();

		GL11.glTranslatef((float) x, (float) y, (float) z);
		

		tile.markForUpdate();
		tile.markDirty();

		renderBlock(tile, tileEnt.getWorldObj(), tileEnt.xCoord,tileEnt.yCoord, tileEnt.zCoord, ModBlocks.flintLathe);
		GL11.glPopMatrix();
	}

	public void renderBlock(TileEntLatheBase tile, World world, int x, int y,int z, Block block) {

		GL11.glPushMatrix();

		float scale = 1.0F;
		GL11.glScalef(scale, scale, scale);

		GL11.glTranslatef(0.5F, 0.5F, 0.5F);
		if(tile.getBlockMetadata()==0||tile.getBlockMetadata()==2){
			GL11.glRotatef(90, 0.0f, 1.0f, 0.0f);
		}

		if(tile.getStackInSlot(0) != null){
			entItem = new EntityItem(tile.getWorldObj(), x, y, z, tile.getStackInSlot(0));
			GL11.glPushMatrix();
			this.entItem.hoverStart = 0.0F;
			RenderItem.renderInFrame = true;
			GL11.glScalef(1.0f, 1.0f, 1.0f);
			if(tile.getStackInSlot(0).getItem().equals(Item.getItemFromBlock(Blocks.wooden_slab)) 
					||tile.getStackInSlot(0).getItem().equals(ModItems.saxleItem)){
				GL11.glRotatef(90f, 1.0F, 0.0F, 0.0F);
			}
			
			
			if(tile.isPowered == true){
				if(tile.getStackInSlot(0).getItem().equals(Item.getItemFromBlock(Blocks.wooden_slab))
						||tile.getStackInSlot(0).getItem().equals(ModItems.saxleItem)){
					GL11.glRotatef(tile.rot, 0.0F, 1.0F, 0.0F);
				}else{
					GL11.glRotatef(tile.rot, 0.0F, 0.0F, 1.0F);
				}
				
				RenderManager.instance.renderEntityWithPosYaw(entItem, 0.0D, -0.223D, 0.0D, 0.0F, 0.0F);

			}
			if(tile.isPowered==false){
				RenderManager.instance.renderEntityWithPosYaw(entItem, 0.0D, -0.223D, 0.0D, 0.0F, 0.0F);
			}

			RenderItem.renderInFrame = false;
			GL11.glPopMatrix();
			tile.markForUpdate();
			tile.markDirty();
		}
		

		FMLClientHandler.instance().getClient().renderEngine.bindTexture(TEXTURE);

		this.model.renderAll();
		GL11.glPopMatrix();
	}

}
