package nmd.primal.energy.render;

import cpw.mods.fml.client.registry.ClientRegistry;
import nmd.primal.energy.render.block.RenderCrank;
import nmd.primal.energy.render.block.RenderLathe;
import nmd.primal.energy.tileents.TileEntCrank;
import nmd.primal.energy.tileents.TileEntLatheBase;

public class RenderRegistry {
	
	public static final void init() {
    	//MinecraftForgeClient.registerItemRenderer(ModItems.woodenShield, new ItemRenderWoodenShield());
    	//MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModBlocks.emptySoftCrucible), new ItemRendererSECrucible());
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntCrank.class, new RenderCrank());
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntLatheBase.class, new RenderLathe());
	}
	
}