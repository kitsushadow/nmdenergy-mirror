package nmd.primal.energy.block.lathe;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import nmd.primal.energy.common.ModInfo;
import nmd.primal.energy.item.ModItems;
import nmd.primal.energy.render.RenderID;
import nmd.primal.energy.tileents.TileEntLatheBase;
import nmd.primal.energy.util.CustomTab;
import nmd.primal.energy.util.EnergyUtil;
import nmd.primal.energy.util.LatheRecipes;

public abstract class LatheBase extends BlockContainer implements ITileEntityProvider{

	private final Random random = new Random();

	protected LatheBase(String unlocalizedName, Material material) {
		super(material);
		this.setBlockName(unlocalizedName);
		this.setBlockTextureName(ModInfo.MOD_ID + ":" + unlocalizedName);
		this.setCreativeTab(CustomTab.NMDEnergyTab);
		this.setHardness(1.0F);
		this.setResistance(6.0F);
		this.setStepSound(soundTypeStone);
		//this.setBlockBounds(0.1F, 0.0F,  0.1F, 
		//		0.9F, 0.75F, 0.9F);
	}

	public boolean onBlockActivated (World world, int x, int y, int z, EntityPlayer player, int q, float a, float b, float c) {

		if(!world.isRemote){
			TileEntLatheBase tileEnt = (TileEntLatheBase) world.getTileEntity(x, y, z);

			if(player.inventory.getCurrentItem()!=null){
				if(player.inventory.getCurrentItem().getItem()!=ModItems.schiselItem){
					if (tileEnt.getStackInSlot(0)==null){
						tileEnt.setInventorySlotContents(0, player.inventory.getCurrentItem());
						player.inventory.setInventorySlotContents(player.inventory.currentItem, null);
						tileEnt.markForUpdate();
						tileEnt.markDirty();
						return true;
					}
					if (tileEnt.getStackInSlot(0)!=null){
						if(player.inventory.getCurrentItem()!=null){
							if(player.inventory.getCurrentItem().getItem()!=ModItems.schiselItem){
								ItemStack pStack = player.inventory.getCurrentItem().copy();
								ItemStack sStack = tileEnt.getStackInSlot(0).copy();
								ItemStack sStackTemp = tileEnt.getStackInSlot(0).copy();
								if(tileEnt.getStackInSlot(0).stackSize < 64){
									sStackTemp.stackSize++;
									if ((sStack.getItem().equals(pStack.getItem())) && (sStack.getItemDamage() == pStack.getItemDamage())  ){
										tileEnt.setInventorySlotContents(0, sStackTemp);
										player.inventory.decrStackSize(player.inventory.currentItem, 1);
										tileEnt.markForUpdate();
										tileEnt.markDirty();
										return true;
									}
								}
							}
						}
					}

				}
			}
			if (player.isSneaking() && player.inventory.getCurrentItem()==null) {
				if(tileEnt.getStackInSlot(0)!=null){
					player.inventory.setInventorySlotContents(player.inventory.currentItem, tileEnt.getStackInSlot(0));
					tileEnt.setInventorySlotContents(0, null);
					tileEnt.markForUpdate();
					tileEnt.markDirty();
					return true;
				}

			}
			if (!player.isSneaking()) {
				if((player.inventory.getCurrentItem()==null)){
					if(tileEnt.getStackInSlot(0)!=null){
						ItemStack pStack = tileEnt.getStackInSlot(0).copy();
						pStack.stackSize = 1;
						world.spawnEntityInWorld(new EntityItem(world, player.posX, player.posY, player.posZ, pStack));
						tileEnt.decrStackSize(0, 1);
						tileEnt.markForUpdate();
						tileEnt.markDirty();
						return true;
					}
				}

			}
			if(player.inventory.getCurrentItem()!=null){
				if(player.inventory.getCurrentItem().getItem()==ModItems.schiselItem){
					if(tileEnt.getStackInSlot(0)!=null){
						if(LatheRecipes.latheCheck.contains(tileEnt.getStackInSlot(0).getItem())){
							tileEnt.workCount++;
							if(tileEnt.workCount>=20){
								tileEnt.setInventorySlotContents(0, LatheRecipes.lathe.get(tileEnt.getStackInSlot(0).getItem()));
								tileEnt.workCount=0;
								tileEnt.markForUpdate();
								tileEnt.markDirty();
							}
						}
					}
				}
			}
		}

		return false;
	}

	public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
		TileEntLatheBase tileEnt = (TileEntLatheBase) world.getTileEntity(x, y, z);

		if (tileEnt != null) {
			for (int i = 0; i < tileEnt.getSizeInventory(); ++i) {
				ItemStack itemstack = tileEnt.getStackInSlot(i);

				if (itemstack != null) {
					float f = this.random.nextFloat() * 0.6F + 0.1F;
					float f1 = this.random.nextFloat() * 0.6F + 0.1F;
					float f2 = this.random.nextFloat() * 0.6F + 0.1F;

					while (itemstack.stackSize > 0) {
						int j = this.random.nextInt(21) + 10;

						if (j > itemstack.stackSize) {
							j = itemstack.stackSize;
						}

						itemstack.stackSize -= j;
						EntityItem entityitem = new EntityItem(world, x + f, y + f1, z + f2, new ItemStack(itemstack.getItem(), j, itemstack.getItemDamage()));

						if (itemstack.hasTagCompound()) {
							entityitem.getEntityItem().setTagCompound(((NBTTagCompound) itemstack.getTagCompound().copy()));
						}

						float f3 = 0.025F;
						entityitem.motionX = (float) this.random.nextGaussian() * f3;
						entityitem.motionY = (float) this.random.nextGaussian() * f3 + 0.1F;
						entityitem.motionZ = (float) this.random.nextGaussian() * f3;
						world.spawnEntityInWorld(entityitem);
					}
				}
			}
			world.func_147453_f(x, y, z, block);
		}

		super.breakBlock(world, x, y, z, block, meta);
	}

	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase player, ItemStack p_149689_6_) {
		int l = EnergyUtil.determineOrientationSimple(player);
		world.setBlockMetadataWithNotify(x, y, z, l, 2);
		System.out.println(l);
		world.markBlockForUpdate(x, y, z);
	}
	
	public void setBlockBoundsBasedOnState(IBlockAccess access, int x, int y, int z)
	{
		int orient = access.getBlockMetadata(x, y, z);
		if(orient==1 || orient == 3){
			this.setBlockBounds(0.2F, 0F, 0F, 0.8F, 0.61F, 1.0F);
		}
		if(orient==0 || orient == 2){
			this.setBlockBounds(0.0F, 0F, 0.2F, 1.0F, 0.61F, 0.8F);
		}
	}
	
	@Override
	public int getRenderType() {
		return RenderID.latheID;
	}
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

}
