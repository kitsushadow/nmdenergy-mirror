package nmd.primal.energy.block.lathe;

import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import nmd.primal.energy.tileents.TileEntLatheBase;

public class FlintLathe extends LatheBase{
	
	public FlintLathe(String unlocalizedName, Material material) {
		super(unlocalizedName, material);

	}
	
	@Override
	public TileEntity createNewTileEntity(World world, int i) {
		return new TileEntLatheBase();
	}
	


}
