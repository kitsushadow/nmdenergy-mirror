package nmd.primal.energy.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import nmd.primal.energy.block.lathe.FlintLathe;
import cpw.mods.fml.common.registry.GameRegistry;

public class ModBlocks {

public static Block SMBBlock;
public static Block crankBlock;
public static Block flintLathe;
	
	public static final void init() {
		
		GameRegistry.registerBlock(SMBBlock = new SMBBlock("SMBBlock", Material.wood), "SMBBlock");
		GameRegistry.registerBlock(crankBlock = new CrankBlock("CrankBlock", Material.wood), "CrankBlock");
		GameRegistry.registerBlock(flintLathe = new FlintLathe("FlintLathe", Material.wood), "FlintLathe");
		//GameRegistry.registerBlock(mineralBlock = new MineralBlock("mineralBlock", Material.rock), "mineralBlock");
	}
}
