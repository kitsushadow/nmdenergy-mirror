package nmd.primal.energy.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import nmd.primal.energy.common.ModInfo;
import nmd.primal.energy.util.CustomTab;

public class SMBBlock extends Block{
	
	protected SMBBlock(String unlocalizedName, Material material) {
		super(material);
		this.setBlockName(unlocalizedName);
		this.setBlockTextureName(ModInfo.MOD_ID + ":" + unlocalizedName);
		this.setCreativeTab(CustomTab.NMDEnergyTab);
		this.setHardness(1.0F);
		this.setResistance(6.0F);
		this.setStepSound(soundTypeStone);
	}
}
