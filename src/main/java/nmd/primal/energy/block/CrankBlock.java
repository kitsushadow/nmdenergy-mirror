package nmd.primal.energy.block;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import nmd.primal.energy.block.lathe.LatheBase;
import nmd.primal.energy.common.ModInfo;
import nmd.primal.energy.render.RenderID;
import nmd.primal.energy.tileents.TileEntCrank;
import nmd.primal.energy.tileents.TileEntLatheBase;
import nmd.primal.energy.util.CustomTab;
import nmd.primal.energy.util.EnergyUtil;

public class CrankBlock extends Block implements ITileEntityProvider{

	protected CrankBlock(String unlocalizedName, Material mat) {
		super(mat);
		this.setBlockName(unlocalizedName);
		this.setBlockTextureName(ModInfo.MOD_ID + ":" + unlocalizedName);
		this.setCreativeTab(CustomTab.NMDEnergyTab);
		this.setHardness(1.0F);
		this.setResistance(6.0F);
		this.setStepSound(soundTypeStone);
	}

	public boolean onBlockActivated (World world, int x, int y, int z, EntityPlayer player, int q, float a, float b, float c) {

		if(!world.isRemote){
			System.out.println(world.getBlockMetadata(x, y, z));
			TileEntCrank tile = (TileEntCrank) world.getTileEntity(x, y, z);
			if(tile.isPowered == false){
				tile.isPowered = true;
			}
			if(tile.getBlockMetadata()==2){
				if(world.getBlock(x, y, z+1) instanceof LatheBase){
					TileEntLatheBase tileLathe = (TileEntLatheBase) world.getTileEntity(x, y, z+1);
					tileLathe.isPowered=true;
					tileLathe.power+=100;
					tileLathe.markDirty();
					tileLathe.markForUpdate();
				}
			}
			tile.markDirty();
			tile.markForUpdate();
			return true;
		}
		return true;

	}

	@Override
	public TileEntity createNewTileEntity(World world, int i) {
		return new TileEntCrank("tileEntCrack");
	}

	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase player, ItemStack stack)
	{
		int l = EnergyUtil.determineOrientationComplex(world, x, y, z, player);
		System.out.println(l);
		world.setBlockMetadataWithNotify(x, y, z, l, 2);
		//}
	}

	public void setBlockBoundsBasedOnState(IBlockAccess access, int x, int y, int z){
		int p = access.getBlockMetadata(x, y, z);
		if(p==0){
			this.setBlockBounds(0.2F, 0.5F, 0.2F, 0.8F, 1.0F, 0.8F);
		}
		if(p==1){
			this.setBlockBounds(0.2F, 0.0F, 0.2F, 0.8F, 0.5F, 0.8F);
		}
		if(p==2){
			this.setBlockBounds(0.2F, 0.2F, 0.5F, 0.8F, 0.8F, 1.0F);
		}
		if(p==3){
			this.setBlockBounds(0.2F, 0.2F, 0.0F, 0.8F, 0.8F, 0.5F);
		}
		if(p==4){
			this.setBlockBounds(0.5F, 0.2F, 0.2F, 1.0F, 0.8F, 0.8F);
		}
		if(p==5){
			this.setBlockBounds(0.0F, 0.2F, 0.2F, 0.5F, 0.8F, 0.8F);
		}
		
	}
	
	@Override
	public int getRenderType() {
		return RenderID.crankID;
	}
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}
	@Override
	public boolean isOpaqueCube() {
		return false;
	}

}
